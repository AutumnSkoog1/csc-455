<?php
/*This code assumes user input is valid and correct only for demo purposes - it does NOT validate form data.*/
	if(!empty($_GET['email'])) { //must have at least a email not = NULL
		$email = $_GET['email'];
		$name = $_GET['business'];
		$pass = $_GET['psw'];
		$pass2 = $_GET['psw-repeat'];
		require_once('../../mysqli_configP.php'); //adjust the relative path as necessary to find your config file
		//Retrieve largest cust_id
		$query = "SELECT MAX(Business_ID) FROM BUSINESS";
		//No prepared statements because nothing is input from user for this query
		$result=mysqli_query($dbc, $query);
		$row=mysqli_fetch_array($result); //enumerated array this time instad of assosciative
		$newID = $row[0] + 1;
		
		$query2 = "INSERT INTO BUSINESS(Business_ID, Email, Business_Name, Password) VALUES (?,?,?,?)";
		$stmt2 = mysqli_prepare($dbc, $query2);
		
		//second argument one for each ? either i(integer), d(double), b(blob), s(string or anything else)
		if( $pass == $pass2){
			mysqli_stmt_bind_param($stmt2, "isss", $newID, $email, $name, $pass);
		}
		else{
			echo "<h2>passwords don't match </h2>";
			echo "<h3><a href='Registration.html'>Please Try Again</a><h3>";
		mysqli_close($dbc);
		} 
		
		if(!mysqli_stmt_execute($stmt2)) { //it did not run successfully
			echo "<h2>We were unable to add the account at this time.</h2>";
			mysqli_close($dbc);
			exit;
		}
		mysqli_close($dbc);
	} 
	else {
		echo "<h2>You have reached this page in error</h2>";
		mysqli_close($dbc);
		exit;
	}
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>LG Co</title>
	<meta charset ="utf-8"> 
</head>
<body>
	<h2>Customer <?php echo "$first_name $last_name";?> was successfully added</h2>
	<h3><a href="add_customer.html">Add another account</a><h3>
	<h3><a href="index.html">Back to Home</a></h3>
</body>
</html>